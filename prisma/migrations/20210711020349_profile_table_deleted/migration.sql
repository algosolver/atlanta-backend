/*
  Warnings:

  - You are about to drop the column `profileId` on the `booking` table. All the data in the column will be lost.
  - You are about to drop the `profile` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `userId` to the `booking` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "booking" DROP CONSTRAINT "booking_profileId_fkey";

-- AlterTable
ALTER TABLE "booking" DROP COLUMN "profileId",
ADD COLUMN     "userId" INTEGER NOT NULL;

-- DropTable
DROP TABLE "profile";

-- AddForeignKey
ALTER TABLE "booking" ADD FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
