/*
  Warnings:

  - You are about to drop the `bookbed` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "bookbed" DROP CONSTRAINT "bookbed_bedId_fkey";

-- DropForeignKey
ALTER TABLE "bookbed" DROP CONSTRAINT "bookbed_bookingId_fkey";

-- DropTable
DROP TABLE "bookbed";
