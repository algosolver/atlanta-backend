import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, Created } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { booking, bookingOnBeds } = new PrismaClient();
export const newBooking = async (req: Request, res: Response) => {
  const {
    userId,
    startDate,
    endDate,
    amount,
    totalPaid,
    adultCount,
    childCount,
    status,
    bedIds,
  } = req.body;
  try {
    const newBooking = await booking.create({
      data: {
        userId,
        startDate,
        endDate,
        amount,
        totalPaid,
        adultCount,
        childCount: childCount ? childCount : 0,
        status: status ? status : 0,
      },
    });

    await bookingOnBeds.createMany({
      data: bedIds.map((bedId: number) => {
        return { bedId, bookingId: newBooking.id };
      }),
    });

    new Created(res, newBooking).serializeResponse();
  } catch (error) {
    console.log('error', error);
    res.status(400).send(error);
  }
};
