import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, NoContent } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { booking } = new PrismaClient();
export const deleteBooking = async (req: Request, res: Response) => {
  try {
    const bookingExists = await booking.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
      select: {
        id: true,
      },
    });
    if (!bookingExists) {
      throw new BadRequestError({ message: 'Booking dose not exist' });
    }
    await booking.delete({
      where: {
        id: parseInt(req.params.id),
      },
    });
    new NoContent(res, {}).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
