import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { booking } = new PrismaClient();

export const findBookingById = async (req: Request, res: Response) => {
  try {
    const prop = await booking.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
    });
    new OK(res, prop).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
