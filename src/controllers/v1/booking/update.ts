import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, NoContent } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { booking } = new PrismaClient();
export const updateBooking = async (req: Request, res: Response) => {
  try {
    const bookingExists = await booking.findFirst({
      where: {
        id: parseInt(req.params.id),
      },
    });
    if (!bookingExists) {
      throw new BadRequestError({ message: 'Booking dose not exist' });
    }
    const { bedIds, ...rest } = req.body;
    const updatedBooking = await booking.update({
      where: {
        id: parseInt(req.params.id),
      },
      data: {
        ...rest,
      },
    });
    new NoContent(res, {}).serializeResponse();
  } catch (error) {
    console.log('error', error);
    res.status(400).send(error);
  }
};
