import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { booking } = new PrismaClient();
export const findBookings = async (req: Request, res: Response) => {
  try {
    const bookings = await booking.findMany();
    res.status(200).send({
      payload: {
        bookings,
      },
    });
    new OK(res, bookings).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
