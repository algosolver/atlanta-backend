import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, Created } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { suite } = new PrismaClient();
export const newSuite = async (req: Request, res: Response) => {
  const { name, status, propertyId, bedCount } = req.body;
  try {
    if (!propertyId) {
      throw new BadRequestError({ message: 'Property id must be provided.' });
    }
    const newSuite = await suite.create({
      data: {
        name,
        propertyId,
        status: status ? status : 1,
        bedCount: bedCount ? bedCount : 0,
      },
    });
    new Created(res, newSuite).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
