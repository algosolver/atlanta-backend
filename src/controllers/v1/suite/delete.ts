import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, NoContent } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { suite } = new PrismaClient();
export const deleteSuite = async (req: Request, res: Response) => {
  try {
    const suiteExists = await suite.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
      select: {
        name: true,
      },
    });
    if (!suiteExists) {
      throw new BadRequestError({ message: 'Suite dose not exist' });
    }
    await suite.delete({
      where: {
        id: parseInt(req.params.id),
      },
    });
    new NoContent(res, {}).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
