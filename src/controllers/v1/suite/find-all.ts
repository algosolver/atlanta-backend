import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { suite } = new PrismaClient();

export const findSuites = async (req: Request, res: Response) => {
  try {
    const allSuites = await suite.findMany({
      where: {
        status: 1,
      },
      include: {
        beds: {
          where: {
            status: 1,
          },
        },
      },
    });
    new OK(res, allSuites).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
