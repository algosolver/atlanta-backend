import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, NoContent } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { suite } = new PrismaClient();
export const updateSuite = async (req: Request, res: Response) => {
  try {
    const suiteExists = await suite.findFirst({
      where: {
        id: parseInt(req.params.id),
      },
      select: {
        name: true,
      },
    });
    if (!suiteExists) {
      throw new BadRequestError({ message: 'Suite dose not exist.' });
    }
    const updatedSuite = await suite.update({
      where: {
        id: parseInt(req.params.id),
      },
      data: {
        ...req.body,
      },
    });
    new NoContent(res, {}).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
