import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { BadRequestError, Created } from '../../../common';
const { suite } = new PrismaClient();

export const bulkInsertSuites = async (req: Request, res: Response) => {
  try {
    if (!req.params.propertyId) {
      throw new BadRequestError({
        message: 'Property id must be provided as a req parms (propertyId)',
      });
    }
    const newSet = req.body.map((item: any, index: any) => {
      return {
        ...item,
        bedCount: item.bedCount ? item.bedCount : 0,
        propertyId: parseInt(req.params.propertyId),
      };
    });
    const bulkSuites = await suite.createMany({
      data: [...newSet],
      skipDuplicates: true,
    });
    new Created(res, bulkSuites).serializeResponse();
  } catch (error) {
    console.log('error', error);

    res.status(400).send(error);
  }
};
