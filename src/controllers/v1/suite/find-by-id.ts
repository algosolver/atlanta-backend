import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { suite } = new PrismaClient();

export const findSuiteById = async (req: Request, res: Response) => {
  try {
    const prop = await suite.findFirst({
      where: {
        id: parseInt(req.params.id),
        status: 1,
      },
      include: {
        beds: {
          where: {
            status: 1,
          },
        },
      },
    });
    new OK(res, prop).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
