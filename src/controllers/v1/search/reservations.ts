import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { booking, suite, bed } = new PrismaClient();

export const reservations = async (req: Request, res: Response) => {
  const { startDate, endDate } = req.body;
  try {
    const bookings = await booking.findMany({
      where: {
        AND: [
          {
            startDate: {
              lte: endDate,
            },
          },
          { endDate: { gte: startDate } },
        ],
      },
      include: {
        beds: {
          select: {
            bed: {
              include: {
                suite: true,
              },
            },
          },
        },
      },
    });

    let bookingArray: any = [];
    bookings.map((booking: any) => {
      let bookingItem = {};
      const { beds, ...restBooking } = booking;
      let su: any = {};
      let bs: any = [];
      beds.map((ele: any) => {
        const { bed } = ele;
        const { suite, ...restBed } = bed;
        bs.push(restBed);
        su = { ...suite, beds: bs };
        bookingItem = { ...restBooking, suite: su };
      });
      bookingArray.push(bookingItem);
    });
    new OK(res, bookingArray).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
