import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { booking, suite, bed } = new PrismaClient();
export const searchBooking = async (req: Request, res: Response) => {
  const { startDate, endDate, suiteId } = req.body;
  try {
    const bookings = await booking.findMany({
      where: {
        AND: [
          {
            startDate: {
              lte: endDate,
            },
          },
          { endDate: { gte: startDate } },
        ],
      },
      select: {
        id: true,
        beds: {
          where: {
            bed: {
              suiteId: suiteId,
            },
          },
          select: {
            bed: {
              select: {
                id: true,
                name: true,
              },
            },
          },
        },
      },
    });

    const ids = bookings
      .map((booking: any) => {
        if (booking.beds.length > 0) {
          return booking.beds.map((bed: any) => bed.bed.id);
        }
      })
      .filter((item) => {
        return item !== undefined;
      });
    const newIds = [].concat(...ids);
    console.log(newIds);

    const beds = await bed.findMany({
      where: {
        AND: [
          { suiteId },
          {
            NOT: { id: { in: newIds } },
          },
        ],
      },
    });

    new OK(res, beds).serializeResponse();
  } catch (error) {
    console.log(error);
    res.status(400).send(error);
  }
};
