import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { BadRequestError, Created } from '../../../common';
import { ErrorMap } from '../../../common/errors/error-map';
const { bed } = new PrismaClient();

export const bulkInsertBeds = async (req: Request, res: Response) => {
  try {
    if (!req.params.suiteId) {
      throw new BadRequestError({
        message: 'Suite id must be provided as a req parms (suiteId)',
      });
    }
    const newSet = req.body.map((item: any, index: any) => {
      return {
        ...item,
        suiteId: parseInt(req.params.suiteId),
      };
    });
    const bulkBeds = await bed.createMany({
      data: [...newSet],
      skipDuplicates: true,
    });
    new Created(res, bulkBeds).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
