import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { bed } = new PrismaClient();

export const findBedById = async (req: Request, res: Response) => {
  try {
    const prop = await bed.findFirst({
      where: {
        id: parseInt(req.params.id),
      },
    });
    new OK(res, prop).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
