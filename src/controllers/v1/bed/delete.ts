import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, NoContent } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { bed } = new PrismaClient();
export const deleteBed = async (req: Request, res: Response) => {
  try {
    const bedExists = await bed.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
      select: {
        name: true,
      },
    });
    if (!bedExists) {
      throw new BadRequestError({ message: 'bed dose not exist' });
    }
    await bed.delete({
      where: {
        id: parseInt(req.params.id),
      },
    });
    new NoContent(res, {}).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};

const router = express.Router();
