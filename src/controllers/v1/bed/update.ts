import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, NoContent } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { bed } = new PrismaClient();

export const updateBed = async (req: Request, res: Response) => {
  try {
    const bedExists = await bed.findFirst({
      where: {
        id: parseInt(req.params.id),
      },
      select: {
        name: true,
      },
    });
    if (!bedExists) {
      throw new BadRequestError({ message: 'Bed dose not exist.' });
    }
    await bed.update({
      where: {
        id: parseInt(req.params.id),
      },
      data: {
        ...req.body,
      },
    });
    new NoContent(res, {}).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
