import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, Created } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { bed } = new PrismaClient();

export const newBed = async (req: Request, res: Response) => {
  const { name, status, suiteId, handicap } = req.body;
  try {
    if (!suiteId) {
      throw new BadRequestError({ message: 'Suite id must be provided.' });
    }
    const newBed = await bed.create({
      data: {
        name,
        suiteId,
        status: status ? status : 1,
        handicap: handicap ? handicap : 0,
      },
    });
    new Created(res, newBed).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
