import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { bed } = new PrismaClient();

export const findBeds = async (req: Request, res: Response) => {
  try {
    const allBeds = await bed.findMany();
    new OK(res, allBeds).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
