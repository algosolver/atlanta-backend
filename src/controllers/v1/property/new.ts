import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, Created } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { property } = new PrismaClient();
export const newProperty = async (req: Request, res: Response) => {
  const { name, status, locationId } = req.body;
  try {
    if (!locationId) {
      throw new BadRequestError({ message: 'Location id must be provided.' });
    }
    const newProperty = await property.create({
      data: {
        name,
        locationId,
        status: status ? status : 1,
      },
    });
    new Created(res, newProperty).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
