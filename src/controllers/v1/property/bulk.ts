import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { BadRequestError, OK } from '../../../common';
const { property } = new PrismaClient();

export const bulkInsertProperties = async (req: Request, res: Response) => {
  try {
    if (!req.params.locationId) {
      throw new BadRequestError({
        message: 'Location id must be provided as a req parms (locationId)',
      });
    }
    const newSet = req.body.map((item: any, index: any) => {
      return {
        ...item,
        locationId: parseInt(req.params.locationId),
      };
    });
    const bulkProperties = await property.createMany({
      data: [...newSet],
      skipDuplicates: true,
    });
    new OK(res, bulkProperties).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
