import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, NoContent } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { property } = new PrismaClient();

export const deleteProperty = async (req: Request, res: Response) => {
  try {
    const propertyExists = await property.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
      select: {
        name: true,
      },
    });
    if (!propertyExists) {
      throw new BadRequestError({ message: 'Property dose not exist' });
    }
    await property.delete({
      where: {
        id: parseInt(req.params.id),
      },
    });
    new NoContent(res, {}).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
const router = express.Router();

router.delete(
  '/api/veterans/property/:id',
  async (req: Request, res: Response) => {}
);
