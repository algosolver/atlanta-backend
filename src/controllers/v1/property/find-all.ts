import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { property } = new PrismaClient();
export const findProperties = async (req: Request, res: Response) => {
  try {
    const allProperties = await property.findMany({
      where: {
        status: 1,
      },
      include: {
        suites: {
          where: {
            status: 1,
          },
          include: {
            beds: {
              where: {
                status: 1,
              },
            },
          },
        },
      },
    });
    new OK(res, allProperties).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
