import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, NoContent } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { location } = new PrismaClient();
export const deleteLocation = async (req: Request, res: Response) => {
  const { name } = req.body;
  try {
    const locationExists = await location.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
      select: {
        name: true,
      },
    });
    if (!locationExists) {
      throw new BadRequestError({ message: 'Location dose not exist' });
    }
    await location.delete({
      where: {
        id: parseInt(req.params.id),
      },
    });
    new NoContent(res, {}).serializeResponse();
  } catch (error) {
    console.log(error);
    res.status(400).send(error);
  }
};
