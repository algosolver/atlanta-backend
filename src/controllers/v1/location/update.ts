import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, NoContent } from '../../../common';
import { PrismaClient } from '@prisma/client';
const { location } = new PrismaClient();
export const updateLocation = async (req: Request, res: Response) => {
  const { name } = req.body;
  try {
    const locationExists = await location.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
      select: {
        name: true,
      },
    });
    if (!locationExists) {
      throw new BadRequestError({ message: 'Location dose not exist' });
    }
    const updatedLocation = await location.update({
      where: {
        id: parseInt(req.params.id),
      },
      data: {
        ...req.body,
      },
    });

    new NoContent(res, updatedLocation).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
