import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { location } = new PrismaClient();
export const findLocations = async (req: Request, res: Response) => {
  try {
    const allLocations = await location.findMany({
      where: {
        status: 1,
      },
      include: {
        properties: {
          where: {
            status: 1,
          },
          include: {
            suites: {
              where: {
                status: 1,
              },
              include: {
                beds: {
                  where: {
                    status: 1,
                  },
                },
              },
            },
          },
        },
      },
    });
    new OK(res, allLocations).serializeResponse();
  } catch (error) {
    console.log(error);
    res.status(400).send(error);
  }
};
