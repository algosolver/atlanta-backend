import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { OK } from '../../../common';
const { location } = new PrismaClient();

export const findLocationById = async (req: Request, res: Response) => {
  try {
    const loc = await location.findFirst({
      where: {
        id: parseInt(req.params.id),
        status: 1,
      },
      include: {
        properties: {
          where: {
            status: 1,
          },
          include: {
            suites: {
              where: {
                status: 1,
              },
              include: {
                beds: {
                  where: {
                    status: 1,
                  },
                },
              },
            },
          },
        },
      },
    });
    new OK(res, loc).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
const router = express.Router();

export { router as findLocation };
