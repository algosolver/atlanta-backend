import express, { Request, Response } from 'express';
import { validateRequest, BadRequestError, Created } from '../../../common';
import { body } from 'express-validator';
import { PrismaClient } from '@prisma/client';
const { location } = new PrismaClient();
export const newLocation = async (req: Request, res: Response) => {
  const { name, status } = req.body;
  try {
    const locationExists = await location.findUnique({
      where: {
        name,
      },
      select: {
        name: true,
      },
    });
    if (locationExists) {
      throw new BadRequestError({ message: 'Location is already exist' });
    }

    const newLocation = await location.create({
      data: {
        name,
        status: status ? status : 1,
      },
    });

    new Created(res, newLocation).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
