import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
import { Created } from '../../../common';
const { location } = new PrismaClient();
export const bulkInsertLoactions = async (req: Request, res: Response) => {
  try {
    const bulkLocations = await location.createMany({
      data: [...req.body],
      skipDuplicates: true,
    });

    new Created(res, bulkLocations).serializeResponse();
  } catch (error) {
    res.status(400).send(error);
  }
};
