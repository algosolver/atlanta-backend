import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { BadRequestError, CustomValidationError, OK } from '../../../common';
import { Password } from '../../../common/services/password';
const { PrismaClient } = require('@prisma/client');

const { user } = new PrismaClient();

export const signin = async (req: Request, res: Response) => {
  const { email, password } = req.body;
  let existingUser = await user.findFirst({
    where: {
      email,
      deletedAt: null,
    },
  });

  if (!existingUser) {
    throw new BadRequestError({ message: 'Invalid credential.' });
  }
  const passwordMatch = await Password.compare(existingUser.password, password);
  if (!passwordMatch) {
    throw new BadRequestError({ message: 'Invalid credential.' });
  }

  // Generate JWT

  const userJwt = jwt.sign(
    {
      id: existingUser.id,
    },
    process.env.JWT_KEY!,
    { expiresIn: 60 * 60 * 24 }
  );

  const { password: pass, ...rest } = existingUser;

  new OK(res, { user: rest, token: userJwt }).serializeResponse();
};
