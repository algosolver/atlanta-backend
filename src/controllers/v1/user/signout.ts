import { Request, Response } from 'express';
import { NoContent } from '../../../common';

export const signout = (req: Request, res: Response) => {
  new NoContent(res, {}).serializeResponse();
};
