import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import {
  BadRequestError,
  Created,
  CustomValidationError,
} from '../../../common';
import { Password } from '../../../common/services/password';
const { PrismaClient } = require('@prisma/client');

const { user } = new PrismaClient();

export const signup = async (req: Request, res: Response) => {
  let { firstName, lastName, dob, email, phone, password } = req.body;
  let existingUser = await user.findFirst({
    where: {
      email,
      deletedAt: null,
    },
  });

  if (existingUser) {
    throw new CustomValidationError({ email: 'Email in use.' });
  }
  const hashedPassword = await Password.toHash(password);

  let newUser = await user.create({
    data: {
      firstName,
      lastName,
      dob,
      email,
      phone,
      password: hashedPassword,
    },
  });

  const userJwt = jwt.sign(
    {
      id: newUser.id,
    },
    process.env.JWT_KEY!,
    { expiresIn: 60 * 60 * 24 }
  );

  const { password: pass, ...rest } = newUser;
  new Created(res, { user: rest, token: userJwt }).serializeResponse();
};
