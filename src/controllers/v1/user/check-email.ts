import { Request, Response } from 'express';
import { BadRequestError, NoContent } from '../../../common';
const { PrismaClient } = require('@prisma/client');

const { user } = new PrismaClient();

export const checkEmail = async (req: Request, res: Response) => {
  const { email } = req.body;
  let existingUser = await user.findFirst({
    where: {
      email,
      deletedAt: null,
    },
  });

  if (existingUser) {
    throw new BadRequestError({ email: 'This email already exists!' });
  }

  new NoContent(res, {}).serializeResponse();
};
