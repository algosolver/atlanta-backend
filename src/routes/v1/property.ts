import express from 'express';
import { body } from 'express-validator';
import { requireAuth, validateRequest } from '../../common';
import {
  bulkInsertProperties,
  newProperty,
  findPropertyById,
  updateProperty,
  deleteProperty,
  findProperties,
} from '../../controllers/v1';

const router = express.Router();

router
  .route('/')
  .get(findProperties)
  .post(
    [body('name').not().isEmpty().withMessage('Name is required')],
    requireAuth,
    validateRequest,
    newProperty
  );

router
  .route('/:id')
  .get(findPropertyById)
  .put(requireAuth, updateProperty)
  .delete(requireAuth, deleteProperty);

router.route('/bulk/:locationId').post();
export { router as propertyRouterV1 };
