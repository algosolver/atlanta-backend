import express from 'express';
import { body } from 'express-validator';
import { requireAuth, validateRequest } from '../../common';
import { searchBooking, reservations } from '../../controllers/v1';

const router = express.Router();

router.route('/bookings').get(searchBooking);
router
  .route('/reservations')
  .get(
    [
      body('startDate').not().isEmpty().withMessage('Start Date is required'),
      body('endDate').not().isEmpty().withMessage('End Date is required'),
    ],
    requireAuth,
    validateRequest,
    reservations
  );

export { router as searchRouterV1 };
