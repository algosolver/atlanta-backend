import express from 'express';
import { body } from 'express-validator';
import { requireAuth } from '../../common';
import { access } from '../../controllers/v1';

const router = express.Router();

router.post('/access', requireAuth, access);

export { router as testRouterV1 };
