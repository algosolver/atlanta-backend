import express from 'express';
import { body } from 'express-validator';
import { requireAuth, validateRequest } from '../../common';
import {
  bulkInsertSuites,
  newSuite,
  findSuiteById,
  findSuites,
  updateSuite,
  deleteSuite,
} from '../../controllers/v1';

const router = express.Router();

router
  .route('/')
  .get(findSuites)
  .post(
    [body('name').not().isEmpty().withMessage('Name is required')],
    requireAuth,
    validateRequest,
    newSuite
  );

router
  .route('/:id')
  .get(findSuiteById)
  .put(requireAuth, updateSuite)
  .delete(requireAuth, deleteSuite);

router.route('/bulk/:propertyId').post();

export { router as suiteRouterV1 };
