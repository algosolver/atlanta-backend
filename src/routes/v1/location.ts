import express from 'express';
import { body } from 'express-validator';
import { requireAuth, validateRequest } from '../../common';
import {
  bulkInsertLoactions,
  newLocation,
  findLocationById,
  updateLocation,
  deleteLocation,
  findLocations,
} from '../../controllers/v1';

const router = express.Router();

router
  .route('/')
  .get(findLocations)
  .post(
    [body('name').not().isEmpty().withMessage('Name is required')],
    requireAuth,
    validateRequest,
    newLocation
  );

router
  .route('/:id')
  .get(findLocationById)
  .put(requireAuth, updateLocation)
  .delete(requireAuth, deleteLocation);

router.route('/bulk/').post(requireAuth, bulkInsertLoactions);

export { router as locationRouterV1 };
