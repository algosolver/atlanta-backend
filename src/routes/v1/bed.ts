import express from 'express';
import { body } from 'express-validator';
import { requireAuth, validateRequest } from '../../common';
import {
  bulkInsertBeds,
  newBed,
  findBedById,
  findBeds,
  updateBed,
  deleteBed,
} from '../../controllers/v1';

const router = express.Router();

router
  .route('/')
  .get(findBeds)
  .post(
    [body('name').not().isEmpty().withMessage('Name is required')],
    requireAuth,
    validateRequest,
    newBed
  );

router
  .route('/:id')
  .get(findBedById)
  .put(requireAuth, updateBed)
  .delete(requireAuth, deleteBed);

router.route('/bulk/:suiteId').post();

export { router as bedRouterV1 };
