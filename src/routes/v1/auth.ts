import express from 'express';
import { body } from 'express-validator';
import { validateRequest } from '../../common';
import { signout, signin, signup, checkEmail } from '../../controllers/v1';

const router = express.Router();

router.post('/logout', signout);
router.post(
  '/login',
  [
    body('email').isEmail().withMessage('Email must be valid.'),
    body('password').trim().notEmpty().withMessage('Password enter password.'),
  ],
  validateRequest,
  signin
);
router.post(
  '/signup',
  [
    body('firstName').notEmpty().withMessage('First name should not be empty.'),
    body('lastName').notEmpty().withMessage('Last name should not be empty.'),
    body('dob').notEmpty().withMessage('DOB should not be empty.'),
    body('email').isEmail().withMessage('Email must be valid.'),
    body('password')
      .trim()
      .isLength({ min: 6, max: 20 })
      .withMessage('Password must be between 6 and 20.'),
  ],
  validateRequest,
  signup
);

router.post(
  '/check-email',
  [body('email').isEmail().withMessage('Email must be valid.')],
  validateRequest,
  checkEmail
);

export { router as authRouterV1 };
