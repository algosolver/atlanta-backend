import express from 'express';
import { body } from 'express-validator';
import { requireAuth, validateRequest } from '../../common';
import {
  deleteBooking,
  findBookingById,
  findBookings,
  newBooking,
  updateBooking,
} from '../../controllers/v1';

const router = express.Router();

router
  .route('/')
  .get(findBookings)
  .post(
    [body('profileId').not().isEmpty().withMessage('Profile Id is required')],
    [
      body('startDate')
        .not()
        .isEmpty()
        .withMessage(
          'Start Date is required in ISO formate(YYYY-MM-DDT00:00:00.000Z)'
        ),
    ],
    [
      body('endDate')
        .not()
        .isEmpty()
        .withMessage(
          'End Date is required in ISO formate(YYYY-MM-DDT00:00:00.000Z)'
        ),
    ],
    [body('amount').not().isEmpty().withMessage('Amount is required')],
    [body('totalPaid').not().isEmpty().withMessage('Total paid is required')],
    [body('bedIds').isArray().withMessage('Beds Id should be an array')],

    [
      body('adultCount')
        .not()
        .isEmpty()
        .withMessage('Number of adutls is required'),
    ],
    requireAuth,
    validateRequest,
    newBooking
  );

router
  .route('/:id')
  .get(findBookingById)
  .put(requireAuth, updateBooking)
  .delete(requireAuth, deleteBooking);

export { router as bookingRouterV1 };
