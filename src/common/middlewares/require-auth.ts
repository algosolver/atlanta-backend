import { Request, Response, NextFunction } from 'express';
import { NotAuthorizedError } from '../errors/not-authorized-error';
import { TokenExpiredError } from '../errors/token-expired-error';

export const requireAuth = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.jwtPayload && Date.now() >= req.jwtPayload!.exp * 1000) {
    throw new TokenExpiredError();
  }
  if (!req.currentUser) {
    throw new NotAuthorizedError();
  }

  next();
};
