import { Request, Response, NextFunction } from 'express';
import { validationResult } from 'express-validator';
import { RequestValidationError } from '../errors/request-validation-error';
import { ErrorMap } from '../errors/error-map';

export const validateRequest = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errorsVal = validationResult(req);

  if (!errorsVal.isEmpty()) {
    let errors: ErrorMap<string> = {};
    errorsVal.array().map((err) => {
      errors[err.param] = err.msg;
    });
    throw new RequestValidationError(errors);
  }

  next();
};
