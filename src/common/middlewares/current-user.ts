import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { TokenExpiredError } from '../errors/token-expired-error';

interface UserPayload {
  id: string;
}
interface JwtExpPayload {
  expiresIn: string;
  exp: number;
}

declare global {
  namespace Express {
    interface Request {
      currentUser?: UserPayload;
      jwtPayload?: JwtExpPayload;
    }
  }
}

export const currentUser = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.header('authorization')) {
    return next();
  }
  try {
    const jwtPayload = jwt.decode(
      req.header('authorization')!
    ) as JwtExpPayload;
    req.jwtPayload = jwtPayload;
    const payload = jwt.verify(
      req.header('authorization')!,
      process.env.JWT_KEY!
    ) as UserPayload;
    req.currentUser = payload;
  } catch (err) {
    console.log(err);
  }

  next();
};
