import { CustomError } from './custom-error';
import { ErrorMap } from './error-map';

export class BadRequestError extends CustomError {
  statusCode = 400;

  constructor(public errors: ErrorMap<string>) {
    super(errors);

    Object.setPrototypeOf(this, BadRequestError.prototype);
  }

  serializeErrors() {
    return this.errors;
  }
}
