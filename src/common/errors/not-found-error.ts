import { CustomError } from './custom-error';
import { ErrorMap } from './error-map';

export class NotFoundError extends CustomError {
  statusCode = 404;

  errors: ErrorMap<string> = { message: 'Not Found' };

  constructor() {
    super({ message: 'Not Found' });
    Object.setPrototypeOf(this, NotFoundError.prototype);
  }

  serializeErrors() {
    return this.errors;
  }
}
