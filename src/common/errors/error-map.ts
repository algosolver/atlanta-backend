export type ErrorMap<T> = { [name: string]: T };
