import { CustomError } from './custom-error';
import { ErrorMap } from './error-map';

export class NotAuthorizedError extends CustomError {
  statusCode = 401;

  errors: ErrorMap<string> = { message: 'Not Authorized' };

  constructor() {
    super({ message: 'Not Authorized' });
    Object.setPrototypeOf(this, NotAuthorizedError.prototype);
  }

  serializeErrors() {
    return this.errors;
  }
}
