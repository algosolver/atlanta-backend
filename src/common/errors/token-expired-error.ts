import { CustomError } from './custom-error';
import { ErrorMap } from './error-map';

export class TokenExpiredError extends CustomError {
  statusCode = 401;

  errors: ErrorMap<string> = { message: 'Token Expired!' };

  constructor() {
    super({ message: 'Token Expired!' });
    Object.setPrototypeOf(this, TokenExpiredError.prototype);
  }

  serializeErrors() {
    return this.errors;
  }
}
