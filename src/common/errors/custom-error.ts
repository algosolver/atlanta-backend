import { ErrorMap } from './error-map';
export abstract class CustomError extends Error {
  abstract statusCode: number;

  constructor(errors: ErrorMap<string>) {
    super(JSON.stringify(errors));
    Object.setPrototypeOf(this, CustomError.prototype);
  }

  abstract serializeErrors(): ErrorMap<string>;
}
