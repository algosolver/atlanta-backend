import { CustomError } from './custom-error';
import { ErrorMap } from './error-map';

export class CustomValidationError extends CustomError {
  statusCode = 400;

  constructor(public errors: ErrorMap<string>) {
    super(errors);
    Object.setPrototypeOf(this, CustomValidationError.prototype);
  }

  serializeErrors() {
    return this.errors;
  }
}
