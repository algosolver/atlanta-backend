import { ValidationError } from 'express-validator';
import { CustomError } from './custom-error';
import { ErrorMap } from './error-map';
export class RequestValidationError extends CustomError {
  statusCode = 400;

  constructor(public errors: ErrorMap<string>) {
    super(errors);

    // Only because we are extending a built in class
    Object.setPrototypeOf(this, RequestValidationError.prototype);
  }

  serializeErrors() {
    return this.errors;
  }
}
