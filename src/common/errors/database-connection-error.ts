import { CustomError } from './custom-error';
import { ErrorMap } from './error-map';

export class DatabaseConnectionError extends CustomError {
  statusCode = 500;
  errors: ErrorMap<string> = { message: 'Error connecting to database' };

  constructor() {
    super({ message: 'Error connecting to database' });
    Object.setPrototypeOf(this, DatabaseConnectionError.prototype);
  }

  serializeErrors() {
    return this.errors;
  }
}
