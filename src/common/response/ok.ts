import { Response } from 'express';
import { CustomResponse } from './response';

export class OK extends CustomResponse {
  statusCode = 200;
  constructor(protected res: Response, protected results: {} | null) {
    super(res, results);
    Object.setPrototypeOf(this, OK.prototype);
  }

  serializeResponse() {
    this.res.status(this.statusCode).send({
      payload: {
        statusCode: this.statusCode,
        success: true,
        results: this.results,
      },
    });
  }
}
