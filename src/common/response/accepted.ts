import { Response } from 'express';
import { CustomResponse } from './response';

export class Accepted extends CustomResponse {
  statusCode = 202;
  constructor(protected res: Response, protected results: {} | null) {
    super(res, results);
    Object.setPrototypeOf(this, Accepted.prototype);
  }

  serializeResponse() {
    this.res.status(this.statusCode).send({
      payload: {
        statusCode: this.statusCode,
        success: true,
        results: this.results,
      },
    });
  }
}
