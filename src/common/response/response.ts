import { Response } from 'express';

export abstract class CustomResponse {
  abstract statusCode: number;
  constructor(res: Response, results: {} | null) {
    Object.setPrototypeOf(this, CustomResponse.prototype);
  }

  abstract serializeResponse(): void;
}
