import { Response } from 'express';
import { CustomResponse } from './response';

export class NoContent extends CustomResponse {
  statusCode = 204;
  constructor(protected res: Response, protected results: {} | null) {
    super(res, results);
    Object.setPrototypeOf(this, NoContent.prototype);
  }

  serializeResponse() {
    this.res.status(this.statusCode).send({
      payload: {
        statusCode: this.statusCode,
        success: true,
      },
    });
  }
}
