import { Response } from 'express';
import { CustomResponse } from './response';

export class Created extends CustomResponse {
  statusCode = 201;
  constructor(protected res: Response, protected results: {}) {
    super(res, results);
    Object.setPrototypeOf(this, Created.prototype);
  }

  serializeResponse() {
    this.res.status(this.statusCode).send({
      payload: {
        statusCode: this.statusCode,
        success: true,
        results: this.results,
      },
    });
  }
}
