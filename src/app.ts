import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';
import { NotFoundError, errorHandler } from './common';
import { currentUser } from './common/middlewares/current-user';
import {
  authRouterV1,
  testRouterV1,
  bedRouterV1,
  bookingRouterV1,
  locationRouterV1,
  propertyRouterV1,
  searchRouterV1,
  suiteRouterV1,
} from './routes/v1';

const app = express();

app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test',
  })
);
app.use(currentUser); //
app.use('/api/v1/auth', authRouterV1);
app.use('/api/v1/beds', bedRouterV1);
app.use('/api/v1/booking', bookingRouterV1);
app.use('/api/v1/locations', locationRouterV1);
app.use('/api/v1/properties', propertyRouterV1);
app.use('/api/v1/search', searchRouterV1);
app.use('/api/v1/suite', suiteRouterV1);
app.use('/api/v1/test', testRouterV1);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };
