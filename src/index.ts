import { Request, Response } from 'express';
import { app } from './app';

const start = () => {
  try {
    app.listen(3000, () => console.log('Server is running on port 3000'));
  } catch (err) {
    console.error(err);
  }
};

start();
